---
title: Call for Proposals to Host useR! 2020
--- 
 
The R Foundation Conference Committee invites proposals to host the useR! 2020 conference, to be held in North America.
 
A distinctive feature of useR! is its emphasis on innovative contribution; as such we require all proposals to have substantial involvement from academic partners.
 
Further details are given below. Please submit your proposals to <R-conferences@r-project.org> by **Friday 16 November 2018**.
 
# Overview
 
## Organizing team
 
The local organizing team will typically comprise members of local universities and possibly other organizations. They are responsible for:
 
- Providing a venue and organizing all facilities required by conference events: furniture, audio-visual equipment, etc.
- Providing catering for conference participants (at least two coffee breaks and one lunch per day, plus at least one evening social/poster event)
- Arranging a conference dinner
- Arranging accommodation or providing information/reductions on accommodation
- Maintaining the conference website
- Managing conference registration, providing conference badges etc.
- Advertising the conference via social media, local societies, etc
- Obtaining sponsorship for the conference and providing promotional opportunities for sponsors, e.g. via conference booths
- Ensuring smooth running of the conference, e.g. organizing a welcome desk, room helpers etc.
- Selecting a program committee and keynote speakers in agreement with the RFCC.
 
It is common to rely on paid support for some or even many of these aspects. For this reason, the “local” team can involve people spread across a wide geographic area, as long as there is sufficient oversight.
 
## Relationship with the R Foundation
 
The local organizing team should maintain regular contact with the RFCC, so that they are aware of progress, particularly regarding the conference program. The organizing committee should include at least one member of the R Foundation -  the RFCC will nominate a member to be added if this is not the case. The program committee should include at least two members of the R Foundation.

The RFCC can provide support by sharing knowledge and materials from previous years (e.g. website template, sponsor contacts that have agreed to their information being passed on, etc). Materials are shared via a GitLab repository that the local organizers should keep up-to-date. The RFCC will also provide input regarding the selection of the program committee and invited contributors. However the local team are responsible for the day-to-day organization.
 
The R Foundation grants the local organizers the right to use the useR! branding. It will encourage R Foundation members to attend the meeting and advertise the meeting on its website, mailing lists and Twitter account. The useR! conference not only provides an event for the R community but also a major source of income for the R Foundation. Therefore the local organizing team is expected to share a reasonable proportion of the profits from the conference with the R Foundation.
 
# Proposal
 
The proposal should be based on 1000 attendees. The general format of recent useR!s should be adopted, but new ideas can be proposed.
 
A proposal will typically include the following:

- An overview of the team: who are the people involved, what is their scientific/technical  background, which universities/businesses/organizations are represented?
- An overview of the host city: why is it suitable for useR!, how easy is it to get to?
- An overview of the proposed venue(s): what are the capacities of each room, how will it accommodate the planned events?
- Potential dates (when venue is free/avoiding major meetings)
- Potential accommodation
- Plans for the general format of the conference and the social program
- Diversity and inclusion considerations: is the venue accessible, will there be on-site child care?
- An outline budget
 
Examples of bids from recent years can be provided on request.
