---
title: Call for Proposals to Host useR! 2021 - Round 2
--- 

Short-listed teams are invited to submit a revised proposal responding to any 
specific feedback from the RFCC and otherwise filling out their proposal as 
guided below, to aid the final selection. The revised proposal should be less 
than 10 pages and submitted to R-conferences@r-project.org by 31 December 2019.

# Revised proposal

The revised proposal should include information on the following aspects, if not 
already covered in the proposal.

## Supporting institution

### Financial support

The hosts are responsible for managing finances and obtaining any necessary 
start-up funding. Please provide a letter of support from 
the institution that will fulfil this role if you are accepted. 

### Legal support

The hosts are responsible for managing legal aspects such as sponsor contracts, 
data protection and handling code of conduct complaints. Please identify if you 
have institutional support for this, or will need to retain outside legal 
counsel.

## Venue

Outline how the venue will accommodate the planned events, including 
room capacities and expected costs. The plan should include:
 
1. One day of satellite events  (2-3 events of <100 people). Events such as the 
tidyverse developer day and Directions in Statistical Computing are often 
organised in partnership with useR!. Could such events be co-located, or hosted 
by a partner institute? What additional costs might this entail?

2. One tutorial day (18-20 half-day tutorials, 750 people).

3. Three conference days (6-7 parallel sessions, keynotes with 1000 people).

It is acceptable for satellite events and tutorials to be hosted outside the 
main venue, but ideally they will be sufficiently close for participants to 
stay in the same accommodation. It is acceptable for keynotes to be split 
across multiple rooms with streaming, but a single room is preferred if an 
affordable option is available.

### Venue accessibility

Please answer the following questions about the venue:

- Are there small rooms available for business meetings/quiet space/nursing/prayer?
- Is there step-free access from the entrance to all rooms, including presenter podiums?
- Are the rooms fitted with an audio induction loop?
- Are there gender-neutral bathrooms, or sufficient bathrooms that some could be labelled as such?
- Are there disabled access bathrooms on each floor, near to the conference rooms?
- Is there designated parking for people with disabilities close to the venue?
- Does the venue permit children and accompanying carers on the premises?
- During what hours could sessions be held? (Flexibility to start early/end late on some days may help to link up with regional hubs).
- Is there any accommodation within 1/4 mile or 400m of the venue, with a step-free route to the venue? (This can make it easier for some disabled participants to attend).

Note that negative responses to any of the questions above will not necessarily discount a venue, but it helps to identify any lack of facilities early on, so that accommodations can be made where possible.

Please supply a map of the venue showing the room layout and an accessibility statement from the venue, if available, as supplementary material.

## Environmental considerations

Ways in which the environmental cost of the conference will be minimised.

## Childcare

Plans for childcare provision for conference participants (on or off site), 
including expected costs.

## Dates

When might the conference be held, taking into account availability of the 
venue, local events and major statistical meetings? Potential dates should be 
in July - September.

## Affordability

Expected typical cost for student/academic/industry participant to attend from 
EU/US/NZ including flight, hotel and registration.

## Provisional budget

The budget should be based on 900 participants paying the proposed regular 
academic registration rate (industry registrations typically cover the student 
discount). Allow costs for 1000 participants (covering free tickets for 
tutors, keynote speakers, organizing and scientific committee members, 
diversity scholars and R Foundation members). Similarly, allow for 700/750 
paying participants for tutorials.

The provisional balance sheet from useR! 2019 is attached to guide your 
budgeting, but please note:

- The total number of participants at useR! 2019 was ~1200 (~1000 paid directly, 
~100 paid via sponsorship).
- The balance is not finalized (management costs are still being negotiated and some sponsorship has not been paid).
- Relevant income/costs can vary depending on the venue/host city and may not be comparable.
- Your provisional budget should not rely on achieving similar levels of sponsorship, but make a conservative estimate to allow for lower income/unexpected costs.

