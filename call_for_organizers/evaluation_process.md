# Evaluation Process

## 2020

The RFCC scored the proposals with a score of 0-5 of how the team had answered the following questions asked in the call for proposals:

An overview of the team: who are the people involved, what is their scientific/technical background, which universities/businesses/organizations are represented?	  
An overview of the host city: why is it suitable for useR!, how easy is it to get to?	  
An overview of the proposed venue(s): what are the capacities of each room, how will it accommodate the planned events?	  
Potential accommodation	  
Plans for the general format of the conference and the social program	  
Diversity and inclusion considerations: is the venue accessible, will there be on-site child care?	  
An outline budget  

This produced a mean score for each candidate from each RFCC member. There was also space for a short review comment, e.g. to add personal knowledge regarding the team, mention things that really stood out in application, issues with proposed dates, major blockers (too expensive, location hard to reach), etc.

Two strong candidates emerged and these were voted on by the full R Foundation membership to select the final candidate by majority vote.

## 2021

### Round 1

The 5 members of the RFCC independently ranked the applications in order of preference, with a short review comment explaining their reasoning.

Issues raised in the comments
- political situation in host country (international relations)
- strength of organizing team (experience in organizing events, connections to R community/useR!, connections to academia, vibrancy of local R community, attention to inclusivity, attention to environmental costs)
- ease of travel to host city (connections, embassy advice on travel to that country, visa policies)
- affordability (of venue/local accomodation)
- appropriateness of facilities (space available)
- potential for local sponsors
- geographical spread of recent useR!s
- added value for local community (strengthening community in under-supported regions)
- local LGBT laws (count out as main venue)
- possibility to be local hub instead
- innovations (e.g. suggestion of small business discount)

Three of the 5 candidates were identified as viable hosts for the main conference. The RFCC could not come to a consensus regarding a preferred hosts and the R Foundation board thought it fair to give them all a chance to put forward more detailed bids, with feedback from the first round.

### Round 2

The RFCC idependently reviewed the updated bids and returned more detailed comments.

Issues raised in the reviews:
- improvements since stage one (e.g. new team members)
- representation (pros and cons of going with a well-established group vs reaching different geographical area)
- links with different communities (e.g. Bioconductor)
- suitabiliy of accommodation for disabled folk
- environmental considerations
- community focus
- issues of building venue up from scratch (in convention centre)
- viability of budget/things missed in the budget (tickets usually given for free to committee members etc)
- proposed dates (overlap with other major conferences)
- oversight required

There were proponents for each of the candidates to be main host. After some debate, a proposal was put forward for two of the candidates to be offered to host in consecutive years (such that useR! would alternate between Europe and North America) and for the third candidate to be offered to be a regional hub in 2022. The third candidate declined on the basis of long-term uncertainty.
