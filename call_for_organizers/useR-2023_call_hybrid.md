---
title: Call for Proposals to Host useR! 2023
--- 

The R Foundation Conference Committee (RFCC) invites proposals to host the useR! 2023 conference. The call for proposals is open to hosts worldwide, deadline **Friday 28 January 2022**.

The useR! conferences are the main meetings for the R user and developer community. There are two main aspects of useR! conferences: 

1. An emphasis on innovative contributions. As such, we require all proposals to have substantial involvement from academic partners and to be led by members of the R community.
2. Having diversity and inclusion at the heart. For that reason, we ask for proposals to put diversity and inclusion practices explicitly at the core of the conference design. 

useR! 2023 will be a **hybrid** conference, enabling participation in person or online.

## Key dates for the proposal process

Interested parties are asked to submit an outline proposal. Based on these proposals, the RFCC will select a short-list of teams, who will be asked to prepare a more detailed bid. The key dates are as follows: 

| Event | Date |
| ----- | ---- |
| Teams submit outline proposal | Friday 28 January 2022 |
| RFCC select short-list | Friday 18 February 2022 |
| Short-listed teams submit detailed bid&nbsp;  | Friday 18 March 2022 |
| R Foundation selects host | Friday 8 April 2022 | 
<br>
Further details are given below. Please submit your outline proposal to <R-conferences@r-project.org> by **Friday 28 January 2022**.

## Essential functions of an organizing team

The organizing team will typically comprise members of universities and possibly other organizations. They are responsible for:

* Establishing the conference program.
* Selecting a program committee and keynote speakers in agreement with the RFCC.
* Maintaining the conference website.
* Managing conference registration, abstract and tutorial submission.
* Advertising the conference via social media, local societies, etc.
* Obtaining sponsorship for the conference and providing promotional opportunities for sponsors, e.g. via conference booths.
* Ensuring that there is a trained code of conduct team to enforce the code of conduct.
* Publishing a [diversity statement](https://rconf.gitlab.io/userknowledgebase/diversity-equity-and-inclusion.html#diversity-statement) and [accessibility guidelines](https://user2021.r-project.org/participation/accessibility/) for presenters on the conference website.
* Organizing social activities during the conference (e.g. a dinner, or online social gatherings).
* For the in-person component:
  * Providing a venue and organizing all facilities required by conference events: furniture, audio-visual equipment, etc.
  * Providing catering for conference participants (at least two coffee breaks and one lunch per day, plus at least one evening social/poster event)
  * Arranging accommodation or providing information/reductions on accommodation
* For the online component:
  * Organizing tools to enable virtual participation.

We recommend that the organizers read the [useR! Knowledgebase](https://rconf.gitlab.io/userknowledgebase/), which provides useful information on organizing a useR! conference. This resource is currently being developed and information is subject to change.

## Outline Proposal

The outline proposal should give an overview of the team's plans for useR! 2023 in no more than 5 pages.

The plans should be based on 500-1000 in-person participants, with a typical useR! format (one day of tutorials, three or four days of conference with plenary sessions, parallel sessions, poster session(s), and one or more social events). Teams are welcome to share any ideas they have for new initiatives.

The proposal should include:
    
1. An overview of the organizing team. 
    * Who are the people leading the bid?
    * What is their scientific/technical background, their career stages?
    * Which universities/businesses/R-communities/organizations are represented? 
    
    We suggest to take a look at the areas of responsibility in the [useR! knowledgebase (Roles and Responsibilities section)](https://rconf.gitlab.io/userknowledgebase/conference-overview.html#roles-and-responsibilities): would the team have people for each of these areas?
    
2. A description of the hybrid mode of the conference, see e.g. [hybrid mode definitions](https://gitlab.com/rconf/userknowledgebase/-/issues/62).
   * Will participants attending online and in-person have access to every part of the program (e.g. keynotes, regular sessions, tutorials, social events)?
   * How do you plan to foster interactions between participants online and on site?
 
3. Initial ideas for the program: do you have ideas for keynote speakers or themes? 

4. Key dates: when would the conference potentially take place? The dates should be in June to September, avoiding major statistical computing meetings and local/major international holidays.

5. General pitch: what are the main reasons to select your proposal, are there other factors that support your bid?


Remember - the following aspects are required: 

  * Involvement of academic institutions and R community members in the organizing team
  * Taking into account diversity and inclusion practices in all spaces of the organization: the organizing team, venue/platforms, ideas for keynotes, budget, and others
  * Including a strong online component at the conference

## Second round

Teams shortlisted on the basis of their outline proposal will be asked to submit a more detailed bid. The required content will be confirmed at that stage, but will include aspects such as:

* Further information regarding the host city, e.g. suitability for participants across all dimensions of diversity, travel connections, affordability. 
* Details of the potential venue(s), e.g. accessibility, room capacities, potential for hosting satellite events.
* Plans to facilitate the virtual component, e.g. how different time zones will be accommodated, which online tools may be used and information on their accessibility.
* Plans for other tools that may be used, e.g. to handle registration/submission, again considering accessibility/usability.
* Plans to minimize environmental impact.
* Plans to directly support inclusion, e.g. childcare.
* Plans for legal and financial support, e.g. managing sponsor contracts, providing a bank account. This is usually provided by a host institution or an events management company.
* Financial plans. Approximate budget: expected costs for venue, audio-visual requirements (including  captioning), catering, code of conduct training, etc. Potential sponsors.
* Draft timeline for organizing the conference. (See example in the [knowledgebase](https://rconf.gitlab.io/userknowledgebase/conference-overview.html#key-dates-for-planning))

## Relationship with the R Foundation Conference Committee (RFCC)

Before submission: The members of the RFCC can be contacted for questions regarding the proposal: <R-conferences@R-project.org>.

During the organization process: The organizing team should maintain regular contact with the RFCC, so that they are aware of progress, particularly regarding the conference program. The RFCC will nominate a delegate to represent them in the organizing team if there is no member of the R Foundation in the team already. 

The RFCC can provide support by sharing knowledge and materials from previous years, though most of it is already in the [useR! Knowledgebase](https://rconf.gitlab.io/userknowledgebase/). The organizers should [update](https://rconf.gitlab.io/userknowledgebase/contributions.html#how-to-contribute-to-this-knowledgebase) the knowledgebase and the official gitlab repository, sharing their experience and material to help future organizers. The RFCC will also provide input regarding the selection of the program committee and keynote speakers, giving the final approval for these aspects. However, the organizing team is responsible for the day-to-day organization.

The R Foundation grants the organizers the right to use the useR! branding. It will encourage R Foundation members to attend the meeting and advertise the meeting on its website, mailing lists and Twitter account. The useR! conference not only provides an event for the R community but also a source of income for the R Foundation. Therefore the organizing team is expected to share a reasonable proportion of the profits from the conference with the R Foundation.

## Relationship with R Core

The useR! conference has a unique opportunity to benefit from contributions from R Core members. One keynote slot is reserved for an R Core speaker and R Core will liaise with the conference organizers regarding other ways that R Core can contribute to the meeting.

