# Meeting on Linux Foundation support for useR! 2021-05-04

Present:

R Consortium: Joe Rickert  
R Foundation/R Consortium: Michael Lawrence  
RFCC: Heather Turner, Martin Maechler, Achim Zeileis  
Linux Foundation: Brian Warner  

Potential directions:

- development of own reviewer tools
- further development of website template, e.g. addition of schedule
- exploration of newer conference management systems, e.g. pretalx, venueless, vito, Matrix (https://matrix.org/blog/2021/02/15/how-we-hosted-fosdem-2021-on-matrix)
    - ISC working group?
- long-term conference assistant

Discussion of R Consortium support for useR! 2021

Requirements for next year
 - What's a hybric conference going to look like?
 - Will we use some of the same tools again? (conftool, Zoom)
 - Need to start discussion with useR! 2022 team

R Consortium support

 - ISC funding could support development project. Proposal can be made directly to the board outside usual cycle [need to confim this, notes unclear!]
 - Would appreciate heads-up of cost structure
     - could be hybrid sponsorships structure: part funding a development project (from ISC funding) and part regular sponsorship (from marketing committee controlled funds)

Linux Foundation support

 - Brian will find out more about what platforms the Linux Foundation uses, what other support the events team could offer.

